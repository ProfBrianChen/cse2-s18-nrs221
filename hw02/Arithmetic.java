/*
Nicholas Silva
CSE2
4/2/18
*/

public class Arithmetic {
  
  public static void main (String [] args) {
	  
	 //Number of pairs of pants
	int numPants = 3;
		
  //Cost per pair of pants
	double pantsPrice = 34.98;
	
  //Number of sweatshirts
	int numShirts = 2;
	//Cost per shirt
	double shirtPrice = 24.99;
	
	
	//Number of belts
	int numBelts = 1;
	
	//cost per box of envelopes
	double beltCost = 33.99;
	
	//the tax rate
	double paSalesTax = 0.06;
    
	  //Total cost of each kind of item (i.e. total cost of pants, etc)
	  double totalCostOfPants = numPants * pantsPrice ; //total cost of pants
	  double totalCostOfShirts = numShirts * shirtPrice ; //total cost of sweatshirts
	  double totalCostOfBelts = numBelts * beltCost; //total cost of belts
	  double totalCostOfAllBeforeTax = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
    
	  //Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
    double salesTaxPants = paSalesTax*totalCostOfPants; //bring back to first line
    double salesTaxShirts = paSalesTax*totalCostOfShirts;
    double salesTaxBelts = paSalesTax*totalCostOfBelts;	
    double totalCostOfAllAfterTax = salesTaxPants+salesTaxShirts+salesTaxBelts; //Total sales tax
    
    
    //Total paid for this transaction, including sales tax. 
    double totalCostOfShirtsWithTax = (totalCostOfShirts*paSalesTax) + totalCostOfShirts;
    double totalCostOfPantsWithTax = (totalCostOfPants*paSalesTax) + totalCostOfPants;
    double totalCostOfBeltsWithTax = (totalCostOfBelts*paSalesTax) + totalCostOfBelts;
		
		double bill = totalCostOfAllAfterTax + totalCostOfAllBeforeTax;
          
    System.out.println("The total cost of pants is: $" + totalCostOfPants + " and the total sales tax is: $" + 
											 String.format("%.2f", salesTaxPants));
    System.out.println("The total cost of sweatshirts is: $" + totalCostOfShirts + " and the total sales tax is: $" + 
                        String.format("%.2f", salesTaxShirts));
    System.out.println("The total cost of belt envelope is:  $" + totalCostOfBelts + " and the total sales tax is: $" +
                        String.format("%.2f", salesTaxBelts));
      
    System.out.println("The total cost of the purchases (before tax) is: $" +
                         totalCostOfAllBeforeTax);
    System.out.println("The total sales tax is: $" + String.format("%.2f", totalCostOfAllAfterTax));
    System.out.println("The total cost of the purchases (including sales tax) is: $" + String.format("%.2f", bill)); 

  }
}
