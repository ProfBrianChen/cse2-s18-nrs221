/*
Nicholas Silva
CSE 2-HW04-Yahtzee.java
2-20-18
*/
import java.util.Scanner;

public class Yahtzee {
  public static void main (String [] args){  
    Scanner input = new Scanner(System.in);
    
    //gives the user the option to either roll randomly or to type in a 5 digit number representing the result of a specific roll
    System.out.println("Would you like to roll randomly (if so, type randmom), or " +
                       "type in a 5 digit numbers representing the result of a specific roll (if so, type manual)");
    String answer = input.nextLine();
    String randomRoll = "random";
    String manualRoll = "manual";
    int dice1, dice2, dice3, dice4, dice5 = 0; //assigning value to the variables
    
    dice1 = (int)(Math.random()*6)+1;
    dice2 = (int)(Math.random()*6)+1;
    dice3 = (int)(Math.random()*6)+1;
    dice4 = (int)(Math.random()*6)+1;
    dice5 = (int)(Math.random()*6)+1;
    
    //if statement that prints out random numbers in it's domain if the user inputs: random
    if( answer.equals("random")){
      System.out.println("Choice: random roll\nYour numbers are: " + dice1+ ", " + dice2 + ", " + dice3 + ", " + dice4 + ", " + dice5 + ".");
    }//or else statement that prompts the user to manually enter desired dice roll values
   else{
      System.out.println("Choice: type in numbers manually\n1st roll: ");
        dice1 = input.nextInt();
      System.out.println("2nd roll: ");
        dice2 = input.nextInt();
      System.out.println("3rd roll: ");
        dice3 = input.nextInt();
      System.out.println("4rth roll: ");
        dice4 = input.nextInt();
      System.out.println("5th roll: ");
        dice5 = input.nextInt();
    }
    
    //boolean has to be created in order to rationalize inputed numbers that are not in the domain for the else statement.
    boolean  numbersOutSideDomain = false;
    //setting paramater
    if( dice1  < 1 || dice1 > 6 ){
      numbersOutSideDomain = true;
    }
    else if( dice2 < 1  || dice2 > 6 ){
      numbersOutSideDomain = true;
    }
    else if( dice3 < 1  || dice3 > 6 ){
      numbersOutSideDomain = true;
    }
    else if( dice4 < 1  || dice4 > 6 ){
      numbersOutSideDomain = true;
    }
    else if( dice5 < 1  || dice5 > 6 ){
      numbersOutSideDomain = true;
    }
   //Outputs wether there is an impossible dice value or that the numners are inside the domain
    if( numbersOutSideDomain == true){
      System.out.println("Dice value is impossible.");
    } else if( numbersOutSideDomain == false){
      System.out.println("There is no number outside of the domain. (No error) ");
    }
    
    //Setting count variables to a value
    int count1, count2, count3, count4, count5, count6; 
    count1 = 0;
    count2 = 0;
    count3 = 0;
    count4 = 0;
    count5 = 0;
    count6 = 0;
    
    //in order to show that the dice is rolled, 1 is added to the count variables
    //dice1
    if( dice1 == 1 ){
      count1 = count1 + 1;
    } else if( dice1 == 2){
      count2 = count2 + 1;
    } else if( dice1 == 3){
      count3 = count3 + 1;
    } else if( dice1 == 4){
      count4 = count4 + 1;
    } else if( dice1 == 5){
      count5 = count5 + 1;
    } else if( dice1 == 6){
      count6 = count6 + 1;
    }//ends dice1
    
    //dice2
    if( dice2 == 1 ){
      count1 = count1+1;
    } else if( dice2 == 2){
      count2 = count2 + 1;
    } else if( dice2 == 3){
      count3 = count3 + 1;
    } else if( dice2 == 4){
      count4 = count4 + 1;
    } else if( dice2 == 5){
      count5 = count5 + 1;
    } else if( dice2 == 6){
      count6 = count6 +1;
    }//end dice2
    
    //dice3
    if( dice3 == 1 ){
      count1 = count1+1;
    } else if( dice3 == 2){
      count2 = count2 + 1;
    } else if( dice3 == 3){
      count3 = count3 + 1;
    } else if( dice3 == 4){
      count4 = count4 + 1;
    } else if( dice3 == 5){
      count5 = count5 + 1;
    }else if( dice3 == 6){
      count6 = count6 +1;
    }//end dice3
    
    //dice4
    if( dice4 == 1 ){
      count1 = count1+1;
    } else if( dice4 == 2){
      count2 = count2 + 1;
    } else if( dice4 == 3){
      count3 = count3 + 1;
    } else if( dice4 == 4){
      count4 = count4 + 1;
    } else if( dice4 == 5){
      count5 = count5 + 1;
    }else if( dice4 == 6){
      count6 = count6 +1;
    }//end dice4
    
    //dice5
    if( dice5 == 1 ){
      count1 = count1+1;
    } else if( dice5 == 2){
      count2 = count2 + 1;
    } else if( dice5 == 3){
      count3 = count3 + 1;
    } else if( dice5 == 4){
      count4 = count4 + 1;
    } else if( dice5 == 5){
      count5 = count5 + 1;
    } else if( dice5 == 6){
      count6 = count6 + 1;
    }//end dice5
   
    int score = 0;
 // only 3 and 4 of a kind and chance will have the score calculated   
    //3 of a kind
    if( count1 == 3 || count2 == 3 ||count1 == 3 || count2 == 3 ||count1 == 3){
      score = (dice1+dice2+dice3+dice4+dice5);  
      System.out.println("3 of a kind: Score = " + score); // double check 
    } 
    //4 of a kind
    else if( count1 == 4 || count2 == 4 ||count1 == 4 || count2 == 4 ||count1 == 4){
      score = (dice1+dice2+dice3+dice4+dice5);  
      System.out.println("4 of a kind: Score = " + score); 
    } 
    //full house
    else if(count1 == 3 && (count2 == 2 || count3 == 2 || count4 == 2 || count5 == 2) || count2 == 3 && (count1 == 2 || count3 == 2 || count4 == 2 || count5 == 2 ) ||
            count3 == 3 && (count1 == 2 || count2 == 2 || count4 == 2 || count5 == 2) || count4 == 3 && (count1 == 2 || count2 == 2 || count3 == 2 || count5 == 2 ) ||
            count3 == 3 && (count1 == 2 || count2 == 2 || count4 == 2 || count5 == 2)){
            System.out.println("full house: Score = 25"); 
    } 
    //small straight
    else if(count1 == 1 && count2 == 1 && count3 == 1 && count4 == 1||
           count2 == 1 && count3 == 1 && count4 == 1 && count5 == 1 ||
           count3 == 1 && count4 == 1 && count5 == 1 && count6 == 1){
      System.out.println("small straight: Score = 30");  
    }
    //large straight
     else if(count1 ==1 && count2 ==1 && count3 ==1 && count4 ==1 && count5 == 1 || 
             count2 ==1 && count3 ==1 && count4 ==1 && count5 ==1 && count6 == 1 ){
      System.out.println("large straight: Score = 40"); 
   }
    //yahtze
     else if(count1 == 5 && count2 == 5 && count3 == 5 && count4 == 5 && count5 == 5){
      System.out.println("yahtze: Score = 50"); 
     } 
    //chance
     else {
      score = (dice1+dice2+dice3+dice4+dice5);  
      System.out.println("Chance: Score = " + score); 
    }
    
}
}