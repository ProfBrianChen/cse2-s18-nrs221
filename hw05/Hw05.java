/*
Nicholas Silva
CSE2 3/6/18
Hw05.java
*/
import java.util.Scanner;

public class Hw05{
  public static void main (String [] args){
    //prompt the user to enter values for each section
    Scanner scan = new Scanner(System.in);
   
    String junkWord;
    
    //check to make sure that the user has entered a value of the correct type with respect to each variable
    //use Scanning methods 
    //while loops for course number
    System.out.println("Enter course number: ");
    while(!scan.hasNextInt()){//if condition is false
      System.out.println("Error, input an integer");// user will be prompted to enter an integer
      junkWord = scan.nextLine();
    }
    int courseNumber = scan.nextInt();// if condition is true, inputed value wll be stored in variable
    //while loop for number of classes
    System.out.println("Enter the number of classess that your class meets a week: ");//# of classes a week (int)
    while(!scan.hasNextInt()){//if condition is false
      System.out.println("Error, input an integer");// user will be prompted to enter an integer
      junkWord = scan.nextLine();
    }
    int numOfClassess = scan.nextInt();// if condition is true, inputed value wll be stored in variable
    
   //while loop for time that class starts
    System.out.println("Enter the time your class starts: ");//time class starts (int)
     while(!scan.hasNextInt()){//if condition is false
      System.out.println("Error, input an integer");// user will be prompted to enter an integer
      junkWord = scan.nextLine();
    }
    int classTime = scan.nextInt();// if condition is true, inputed value wll be stored in variable
   
   //while loop for number of students
    System.out.println("Enter the number of students in your class: ");//# of students (int)
    while(!scan.hasNextInt()){//if condition is false
      System.out.println("Error, input an integer");// user will be prompted to enter an integer
      junkWord = scan.nextLine();
    }
    int numOfStudents = scan.nextInt();// if condition is true, inputed value wll be stored in variable
   

    // while loops for department name
    System.out.println("Enter department name: ");//department name (string)
    while(scan.hasNextDouble()){//if condition is true. different then other boolean conditions because now we want to the error to be in the form of an int
    System.out.println("Error, input an integer");
    junkWord = scan.nextLine();
    }
    String departmentName = scan.nextLine();// if condition is true, inputed value wll be stored in variable
    
    //while loop for instructor's name
    System.out.println("Enter your instructor's name: ");//instructor's name (string)
    while(scan.hasNextDouble()){
    System.out.println("Error, input a string");
    junkWord = scan.nextLine();
    }
    String instructorsName = scan.nextLine();// if condition is true, inputed value wll be stored in variable
    
    
    //prints out all inputted values
    System.out.println("Course number: " + courseNumber + " Number of classes: " + numOfClassess + " Number of students: " +
                      numOfStudents + " Department name: " + departmentName + " Instructor's name: " + instructorsName);
    
  }
}