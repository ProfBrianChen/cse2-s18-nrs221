import java.util.Scanner;

public class Check {
  public static void main (String [] args) {
  Scanner input = new Scanner(System.in);//imports method that lets the user input 
  System.out.print("Enter the original cost of the check in the form xx.xx: ");
  double checkCost = input.nextDouble();//sets whatever is inputed into the variable checkCost
  System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
  double tipPercent = input.nextDouble();//sets whatever is inputed into the variable tipPercent
  tipPercent /= 100; //We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner: ");
  int numPeople = input.nextInt();//sets whatever is inputed into the variable numPeople
 
 double totalCost = checkCost * (1 + tipPercent);
  double costPerPerson = totalCost / numPeople;
  int dollars,   //whole dollar amount of cost 
      dimes, 
      pennies; //for storing digits
          //to the right of the decimal point 
          //for the cost$ 
 
  //get the whole amount, dropping decimal fraction
  dollars=(int)costPerPerson;
  //get dimes amount, e.g., 
  // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
  //  where the % (mod) operator returns the remainder
  //  after the division:   583%100 -> 83, 27%5 -> 2 
  dimes=(int)(costPerPerson * 10) % 10;
  pennies=(int)(costPerPerson * 100) % 10;
  
  System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);






  }
}