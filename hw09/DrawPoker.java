/*Nicholas Silva
CSE 2 
HW09
*/
public class DrawPoker {
    
  public static boolean pair(int[] hand) {
    for (int i = 1; i < hand.length; i++) {
      if (hand[i] % 13 == hand[0] % 13 ) {
        return true;
      }
    } 
    return false;
  }
    
  public static boolean threeOfAKind(int[] hand) {
    for (int i = 1; i < hand.length; i++) {
      for (int k = i + 1; k < hand.length; k++) {
        if (hand[0] % 13 == hand[i] % 13 && hand[0] % 13 == hand[k] % 13 ) {
          return true;
        }
      }
    } 
    return false;
  }

  public static boolean flush(int[] hand) {
    int counter = 0;
    for (int i = 1; i < hand.length; i++) {
      if (hand[0] / 13 == hand[i] / 13) {
        counter += 1;
      }
    }
    if (counter == 4) {
      return true;
    }
    return false;
  }

  public static boolean fullHouse(int[] hand) {
    if ( pair(hand) && threeOfAKind(hand) ) {
      return true;
    }
    return false;
  }
  public static void shuffle(int[] card) {//this method shuffles the deck
    for (int i = 0; i < card.length; i++) {
      int rand = (int) (Math.random() * card.length);
      int temp = card[i];
      card[i] = card[rand];//reorganizing the new deck array
      card[rand] = temp;
    }
  }
  public static void main(String[] args) {
    
    int[] deck = new int[52]; //deck of 52 cards
    for (int i = 0; i < 52; i++) { 
      deck[i] = i; //assing each card to an index in the new array
    }
    shuffle(deck); //shuffle deck 


    int [] p1 = new int[5]; //hand 1 has 5 cards represented by ints
    int [] p2 = new int[5]; //hand 2 has 5 cards represented by ints
    
    for (int x = 0; x < 5; x++) {
      p1[x] = deck[2*x];
      p2[x] = deck[2*x + 1];
    }

    System.out.println("1st player's hand: "); //prints out hands 
    for (int x : p1) {
      System.out.print(x + "\t");
    }

    System.out.println("2nd player's hand: ");
    for (int x : p2) {
      System.out.print(x + "\t");
    }

    int p1points = 0; 
    int p2points = 0;
    //player1
    if (pair(p1)) {
      p1points =p1points+ 1;
    }
    if (threeOfAKind(p1) ) {
      p1points =p1points+ 2;
    }
     if (flush(p1) ) {
      p1points =p1points+ 4;
    }
    if (fullHouse(p1) ) {
      p1points  = p1points + 8;
    }
    //player2
    if (pair(p2) ) {
      p2points =p2points+ 1;
    }
    if (threeOfAKind(p2) ) {
      p2points =p2points + 2;
    }
    if (flush(p2) ) {
      p2points =p2points + 4;
    }
    if (fullHouse(p2) ) {
      p2points =p2points + 8;
    }
    //are the draws the same
    if (p1points == p2points) {
      if ( compare(p1, p2) ) {
        p1points =p1points+ 1;
      }
      else {
        p2points =p2points + 1;
      }
    }
//prints out the winner
    if (p1points > p2points) {
     
      System.out.println("Winner is Player 1");
    }
    else {
      System.out.println("Winner is Player 2");
    }
  }


  

  public static boolean compare(int[] hand, int[] hand1) {
    int max = hand[0];
    for (int i = 0; i < hand1.length; i++) {
      if (hand[i] > max) {
        max = hand[i];
      }
    }
    int max2 = hand1[0];
    for (int i = 0; i < hand1.length; i++) {
      if (hand1[i] > max2) {
        max2 = hand1[i];
      }
    }
    if (max > max2) {
      return true;
    }
    return false;
  }
}