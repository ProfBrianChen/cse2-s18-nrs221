/*
Nicholas Silva
CSE2
2/2/18
*/

// Document your program. What does MPG do? Place you comments here!
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  //number of secs in trip 1
      int secsTrip2=3220;  //number of secs in trip 2
	    int countsTrip1=1561;  //number of counts in trip 1 
	    int countsTrip2=9037; //number of counts in trip 2

      double wheelDiameter=27.0,  //diameter of wheel and all of its components
  	  PI=3.14159, //value of pi
    	feetPerMile=5280,  //number of feetPerMile
    	inchesPerFoot=12,   //number of inchesPerFoot
  	  secondsPerMinute=60;  //number of secondsPerMinute
	    double distanceTrip1, distanceTrip2, totalDistance;  //shortcut

       System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles 
      // calculates distanceTrip1 and reassigns its value to distanceTrip1 so that its in miles
      
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

      
	}  //end of main method   
} //end of class

