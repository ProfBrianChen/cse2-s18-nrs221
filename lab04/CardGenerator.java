/*Nicholas Silva
CSE2
2/16/18
*/

/*After you generate a random number, create two String variables: a String corresponding to the name of the cardSuit and a String corresponding to the identity of the card.
Use if statements to assign the cardSuit name. 
Use a switch statement to assign the card identity.
Print out the name of the randomly selected card.*/

import java.util.Random; 
public class CardGenerator {
  public static void main (String [] args){
  // generate random number from 1 to 52
  int card = (int) (Math.random()* 51) + 1; // upper limit is 52 and lower limit is 1. 
    String cardNumber;
    String cardSuit = "";
    
    //diamonds range
    if( 1 <= card && card <= 13 ){
      cardSuit = "diamonds";
    }
    //clubs range
    else if( 14 <= card && card <= 26 ){
      cardSuit = "clubs";
      card = card - 13; //fixes the number in order so that the cardNumber could be assigned to be either jack, queens, kings, ace, or just a number
    }
    //hearts range
    else if( 27 <= card && card <= 39 ){
      cardSuit = "hearts"; 
      card = card - 26;//fixes the number in order so that the cardNumber could be assigned to be either jack, queens, kings, ace, or just a number
    }   
    //spades range
    else if( 40 <= card && card <= 52 ){
      cardSuit = "spades";
      card = card - 39;//fixes the number in order so that the cardNumber could be assigned to be either jack, queens, kings, ace, or just a number
      
    }
    if(2 <= card && card <= 10){
      cardNumber = "" + card;
    }
    else if( card == 11 ){
      cardNumber = "jack";
    }
    else if( card == 12 ){
      cardNumber = "queens";
    }
    else if( card == 13 ){
      cardNumber = "kings";
    }
    else{
      cardNumber = "ace";
    }
    System.out.println("You picked the " + cardNumber + " of " + cardSuit);
 }

}