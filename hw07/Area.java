/*
  Nicholas Silva
  cse 2 hw07
  program 1
*/

import java.util.Scanner;

public class Area {    
    public static String checkInput (){ //method that checks the input for the correct shape
    
      Scanner scan = new Scanner(System.in);
      System.out.println("Enter: rectangle, triangle or circle");//shapes to choose from
      String shape = scan.next(); //inputed shape gets stored in String type
      boolean check = true;
      while(check){ //will always run because boolean is initialized as true
        if (shape.equals("rectangle")){
          break;
        }
        else if (shape.equals("triangle")){
          break;
        }
        else if (shape.equals("circle")){
          break;
        }
        else {
            System.out.println("Wrong input, try again. Enter: rectangle, triangle or circle");//prompts user to input correct shape
            shape = scan.next();
        }  
       }//checkInput while loop ends
        return shape;
     }//check method that validates the chosen shape
 
  public static double rectangle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double length = scan.nextDouble();
    
    System.out.println("width: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double width = scan.nextDouble();
    
    double area = length * width;
    return area;
  }//rectangle method ends
  
  
   public static double triangle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("length: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double length = scan.nextDouble();
    
    System.out.println("base: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double base = scan.nextDouble();
    
    double area = (length * base)/2;
    return area;
  }//trinagle method ends
  
  
   public static double circle (){
    Scanner scan = new Scanner(System.in);
    System.out.println("radius: ");
    while(!scan.hasNextDouble()){
      System.out.println("Error, input a double");
      String junkWord = scan.nextLine();
    }
    double radius = scan.nextDouble();
    
    double area = Math.pow(radius,2)*3.14;
    return area;
  }//cirlce method ends
  
   public static void main(String [] args){
     
    String shape = checkInput() ;//shape that will be used 
    double area = 0;
    if (shape.equals("rectangle")){
          area = rectangle();//assigns value stored in rectangle method to area
        }
        else if (shape.equals("triangle")){
          area = triangle();
        }
        else if (shape.equals("circle")){
          area = circle();
        }
    System.out.println("Area of " + shape + " is: " + area );
  }//main method ends
  
}//class

 