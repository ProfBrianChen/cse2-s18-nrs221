import java.util.Arrays;
import java.util.Scanner;
public class CSE2Linear{
	public static void linear(int[] array, int x)// LinearSearch method
    {
        for (int i = 0; i < array.length; i++){
            if (array[i] == x){//is number = position?
                
                System.out.println(x + " was found in the list in " + i + 1 + " iterations");
                break;
            }
            else if (i == (array.length - 1)&& array[i] != x ){
                System.out.println(x + " was not found in the list with" + i + 1 + " iterations");
            }
        }
    }
	 public static void binary(int[] ar, int num){
	        int lowestIndex = 0;
	        int highestIndex = 14;
	        int i = 0;
	        while (highestIndex >= lowestIndex){
	        	i++;
	            int midIndex = (highestIndex + lowestIndex)/2;
	            if (num < ar[midIndex]){
	            	highestIndex = midIndex - 1;
	            }
	            else if (num > ar[midIndex]){
	            	lowestIndex = midIndex + 1;   
	            }
	            else if (num == ar[midIndex]){
	                System.out.println(num + " was found in " + i + " iterations");
	                break;
	            }
	        }
	        if (highestIndex < lowestIndex ){
	            System.out.println(num + " was not found in " + i + " iterations");
	        }
	    }
	public static int[] scramble(int[] arrayy){//scramble method
        for (int i = 0; i < arrayy.length; i++){
            int a = (int)(Math.random()*arrayy.length);// random number 
            int temp = arrayy[i];
            while (a != i){//they cannot equal each other
                arrayy[i] = arrayy[a]; arrayy[a] = temp;
                break;
            }
        }
        return arrayy;
    }
   
    public static void main (String[]args){
        Scanner input = new Scanner(System.in);//initializing the scanner

        System.out.println("Enter 15 ascending ints for final grades in CSE2:");// prompts the user to enter 15 integers
        int i = 0;
        int[] arrr = new int[15];
        while (i < 15){
            if (input.hasNextInt()){//is input an int?
            	arrr[i] = input.nextInt();
                if (arrr[i] < 0 || arrr[i] > 100){//checks if int is within the range
                    System.out.println("Error: out of range");
                }
                else{
                    if (i > 0){
                        if (arrr[i] < arrr[i - 1]){
                            System.out.println("You didn't enter an intger greater than the last one ");
                        }
                    }
                i++;
                }
            }
            else{
                System.out.println("You didn't enter an integer");
            }
        }
        
        System.out.println(Arrays.toString(arrr));// method that converts an array so that it can be printed out 
        System.out.print("Enter a grade to search for: ");
        if (input.hasNextInt()){
            int x = input.nextInt();
            binary(arrr, x);
            scramble(arrr);
            System.out.println("Scrambled:");
            System.out.println(Arrays.toString(arrr));// converts array to string
            System.out.print("Enter a grade to search for: ");
            if (input.hasNextInt()){
                linear(arrr,x);
            }
            else{
                System.out.println("No integer was inputted");
                System.exit(0);
            }
        }
        else{
            System.out.println("No grade was inputed");
        }
    }
    
    
    
}