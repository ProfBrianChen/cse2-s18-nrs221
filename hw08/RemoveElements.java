import java.util.Scanner;
public class RemoveElements{
  public static int[] randomIputs(){//method that produces a ramdom value
    int number[]=new int[10];
    for (int i = 0; i < number.length; i++){
      int x = (int)(Math.random()*10);
      number[i] = x;
    }
    return number;
  }
  
  public static int[] delete(int[]array, int y){
    int [] array2 = new int[9];// make a new array of length 9 
    for (int j = 0; j < y; j++)// copies all of the original array until it reaches the indicated index
    {
      array2[j] = array[j];
    }
    for (int i = y + 1; i < array.length; i++){//moves indexes to a new array
      array2[y] = array[i];
      y++;
    }
    return array2;
  }
 
  public static int[] remove(int[]array, int y){
    int a = 0;
    for (int i = 0; i < array.length; i++){//array length rises whenever there is nothing found 
        a++;
      }
    int array2[] = new int [a];
    int a1 = 0;//set to 0
    for (int j = 0; j < array.length; j++){
      if (array[j] != y)//case
      {
        array2[a1] = array[j];
        a1++;
      }
    }
    return array2;
  }
  public static void main(String [] arg){
    Scanner input=new Scanner(System.in);
    int number[]=new int[10];
    int array1[];
    int array2[];
    int index,target;
      String answer="";
      do{
        System.out.println("randomized input ten ints between zero and nine");
        number = randomIputs();
        
        System.out.println("list looks like this:" + arrayList(number));

        System.out.print("pos is ");
        index = input.nextInt();
        array1 = delete(number,index);
        String out1="output array: ";
        out1 = out1 + arrayList(array1);   
        System.out.println(out1);

          System.out.print("delete method generates a new array that looks like this: ");
        target = input.nextInt();
        array1 = remove(number,target);
        String out2="output array: ";
        out2+=arrayList(array1);   
        System.out.println(out2);

        System.out.print("retry");
        answer=input.next();
      }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String arrayList(int numero[]){
      String out= "{";
      for(int k=0;k<numero.length;k++){
        if(k>0){
          out= out + ", ";
        }
        out= out + numero[k];
      }
      out = out + "} ";
      return out;
   }  
}
 
