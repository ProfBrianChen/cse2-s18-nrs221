import java.util.Scanner;

public class Convert{
  public static void main (String [] args) {
    Scanner input = new Scanner(System.in);
    
    //asks user to input values for numOfAcres and inchesOfRain
    System.out.print("Enter the affected area in acres: ");
    double numOfAcres = input.nextDouble();//inputted double stores number of acres in numOfAcres variable
    System.out.print("Enter the rainfall in the affected area (inches): ");
    double inchesOfRain = input.nextDouble(); //inputted double stores inches of rain in inchesOfRain variable
  
    //conversion factors
    double squareMilesPerAcre = 0.0015625;//square Miles Per Acre conversion factor
    double inchesPerMile = 63360;//inches per mile conversion factor
   
    //converting and asigning values
    double areaM = numOfAcres*squareMilesPerAcre, //area in miles
    rainM = inchesOfRain/inchesPerMile, // quantity of rain in terms of miles
    
    // calculates sqaure miles of rain
    squareMilesOfRain = areaM *rainM; 
    
 
    System.out.println(squareMilesOfRain + " cubic miles");// prints out how many cubic miles
    
    
  }
}