import java.util.Scanner;

public class Pyramid {
  public static void main (String [] args) {
    Scanner input = new Scanner(System.in);
    
    System.out.print("The square side of the pyramid is (input length): ");
    double length = input.nextDouble();//assigns inputted value to variable length
    System.out.print("The height of the pyramid is (input height): ");
    double height = input.nextDouble();//assigns inputted value to variable height
    double volume = (length * length * height)/3;//calculates volume
    System.out.println("The volume inside the pyramid is: " + volume);//prints out the volume inside the pyramid
  }
}