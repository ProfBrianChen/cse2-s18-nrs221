import java.util.Scanner; //Scanner is in the java.util package
public class test{
  public static void main (String[] args) {
    //create a scanner object
    Scanner input = new Scanner(System.in);
    //prompt the user to pick a number between 0 and 100
    System.out.println("Enter a numerical grade between 0 and 100");
    double testScore = input.nextDouble();
    char grade;
  
    if (testScore >= 90) {
        grade = 'A';
    } else if (testScore >= 80) {
        grade = 'B';
    } else if (testScore >= 70) {
        grade = 'C';
    } else if (testScore >= 60) {
        grade = 'D';
    } else {
        grade = 'F';
    }
    System.out.println("Grade = " + grade);
    }
    
}