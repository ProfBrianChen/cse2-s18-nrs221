

import java.util.Scanner;

public class TwistGenerator {
  public static void main(String[] args){
    int length = 0;
    int a = 0;
    int b = 0;
    int c = 0;
    Scanner scan = new Scanner(System.in);
    
    while (length < 1){
      System.out.print("Enter a positive number");
      if(scan.hasNextInt() == true){
        length = scan.nextInt();
      }
      else {
        String junkWord = scan.next();
      }
    }
    
    int remainder = length % 3;
    
    String topLayer1 = "\\" ;
    String topLayer2 = " " ;
    String topLayer3 = "/" ;
    
    String midLayer1 = " ";
    String midLayer2 = "x";
    String midLayer3 = " ";
    
    String bottomLayer1 = "/";
    String bottomLayer2 = " ";
    String bottomLayer3 = "\\";
    
    String topSeg = "";
    while ( a < length){
      topSeg = topSeg.concat(topLayer1);
      a++;
      if (a == length){
        break;
      }
      topSeg = topSeg.concat(topLayer2);
      a++;
      if (a == length){
        break;
      }
      topSeg = topSeg.concat(topLayer3);
      a++;
      if (a == length){
        break;
      }
    }
    String midSeg = "";
    while ( b < length){
      midSeg = midSeg.concat(midLayer1);
      b++;
      if (b == length){
        break;
      }
      midSeg = midSeg.concat(midLayer2);
      b++;
      if (b == length){
        break;
      }
      midSeg = midSeg.concat(midLayer3);
      b++;
      if (b == length){
        break;
      }
    }
    String botSeg = "";
    while ( c < length){
      botSeg = botSeg.concat(bottomLayer1);
      c++;
      if (c == length){
        break;
      }
      botSeg = botSeg.concat(bottomLayer2);
      c++;
      if (c == length){
        break;
      }
      botSeg = botSeg.concat(bottomLayer3);
      c++;
      if (c == length){
        break;
      }
    }
      
    System.out.println( topSeg );
    System.out.println( midSeg );
    System.out.println( botSeg );
    
  } // end of method
} // end of class

