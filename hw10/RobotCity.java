//Nicholas Silva, CSE 2, HW10, RobotCity.java
public class RobotCity{
  
  //method that sets the city dimensions
  public static int [][] buildCity(){
    int rows = (int )(Math.random() * 15 + 10);
    int columns = (int )(Math.random() * 15 + 10);
    int [][] array = new int[rows][columns];
    //for loop that populates the city blocks into each city block
    for(int i = 0; i <array.length; i++){
      for(int k = 0; k  <array[i].length;k++){
        array[i][k] = (int )(Math.random() * 900 + 100);
      }
      System.out.println();/*skips a line so that
                            whenever the first row is complete, 
                            the second row goes gets filled*/
    }
    return array;//returns the city dimensions
  }
  //method that makes blocks in the city grid negative
  public static int [][] invade(int [][] cityInvasion, int a){
    boolean x = true;
    int left_right, up_down;
    for(int i =0; i < a; i++){
      do{
        x = true;
        left_right = (int)(Math.random()* cityInvasion.length);
				up_down = (int)(Math.random()* cityInvasion[left_right].length);
				if (cityInvasion[left_right][up_down] > 0)
				{
					cityInvasion[left_right][up_down] *= -1;
					x = false;
				}
      }
      while(x == true);
    }
    return cityInvasion;
  }
  //updates the previous grid
  public static int [][] update(int [][] cityy){
    int[][] invadedCity = new int[cityy.length][cityy[0].length];
    for(int i =0; i<cityy.length;i++){
      for(int x = 0; x< cityy[i].length; x++){
        invadedCity[i][x] = cityy[i][x];
      }
    }
    for(int i = 0; i < cityy.length; i++)
		{
			for (int x = 0; x < cityy[i].length; x++)
			{
				if (cityy[i][x] < 0)
				{
					invadedCity[i][x] *= -1;
					if (x+1 < cityy[i].length && cityy[i][x+1] > 0)
						invadedCity[i][x+1] *= -1;
				}
			}
		}
		return invadedCity;
	}
  
  
  public static void main (String[] args){
   	int[][] cityMatrix = buildCity();
		display(cityMatrix);
		int x = (int)(Math.random()*50)+1;
		cityMatrix = invade(cityMatrix, x);
		display(cityMatrix);
		for (int i = 0; i < 5; i++)
		{
			cityMatrix = update(cityMatrix);
			display(cityMatrix);
		}
    
  }
  //method that displays the grid
  public static void display(int [][] cityMatrix){
    System.out.println();
    for(int i = 0; i < cityMatrix.length; i++){
      for (int k = 0; k < cityMatrix[k].length; k++){
        System.out.print(cityMatrix[i][k] + " ");
      }
      System.out.println();
    }

  }

}